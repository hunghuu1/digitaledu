function ScrollToElement (idscroll,idmenu){
    if(typeof idscroll === "string" && typeof idmenu === "string"){

        let scroll = document.querySelector(idscroll);
        let menu = document.querySelector(idmenu);

        if(scroll&&menu){
            scroll.addEventListener("click", function(){
                menu.classList.toggle('active')
            });
        }
    }
}

ScrollToElement('#menu','#scroll');


function menuMobile( idmenu, idmobile){

    if (typeof idmenu === "string" && typeof idmobile === "string"){
        let menu = document.querySelector(idmenu);
        let mobile = document.querySelector(idmobile);

        if ( menu && mobile){
            menu.addEventListener("click", function(){
                mobile.scrollIntoView({
                    behavior: "smooth", 
                    block: "start", 
                    inline: "nearest",
                });
            })
        }
    }
}

menuMobile('#all', '#mobile');

menuMobile( '#item', '#mobile' );
